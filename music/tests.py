from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.utils import json
from rest_framework.views import status
from django.contrib.auth.models import User

from .models import Songs
from .serializers import SongSerializer


class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_song(title="", artist=""):
        if title != "" and artist != "":
            Songs.objects.create(title=title, artist=artist)

    def login_a_user(self, username="", password=""):
        url = reverse(
            "auth-login",
            kwargs={
                "version": "v1"
            }
        )
        return self.client.post(
            url,
            data=json.dumps({
                "username": username,
                "password": password
            }),
            content_type="application/json"
        )

    def login_client(self, username="", password=""):
        # get token from DRF
        response = self.client.post(
            reverse('create-token'),
            data=json.dumps({
                'username': username,
                'password': password
            }),
            content_type='application/json'
        )
        self.token = response.data['token']
        # set token header
        self.client.credentials(
            HTTP_AUTTHORIZATION='Bearer ' + self.token
        )
        self.client.login(username=username, password=password)
        return self.token

    def setUp(self):
        self.user = User.objects.create_superuser(
            username="test_user",
            email="test@gmail.com",
            password="testing",
            first_name="test",
            last_name="user"
        )

    # def setUp(self):
    #     self.create_song("Like Glue", "sean paul")
    #     self.create_song("simple song", "konshens")
    #     self.create_song("love is wrecked", "brick and lace")
    #     self.create_song("jam rock", "damien marley")


class AuthLoginUserTest(BaseViewTest):

    def test_login_user_with_valid_credentials(self):
        response = self.login_a_user("test_user", "testing")
        self.assertIn("token", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.login_a_user("anonymous", "pass")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class GetAllSongsTest(BaseViewTest):

    def test_get_all_songs(self):
        """
        This test ensures that all songs added in the setUp method
        exist when we make a GET request to the songs/ endpoint
        """

        # hit the API endpoint
        # response = self.client.get(reverse("songs-all", kwargs={"version": "v1"}))

        # fetch the data from database
        # expected = Songs.objects.all()
        # serialized = SongSerializer(expected, many=True)
        # self.assertEqual(response.data, serialized.data)
        # self.assertEqual(response.status_code, status.HTTP_200_OK)

        # this is the update you need to add to the test, login
        self.login_client('test_user', 'testing')
        # hit the API endpoint
        response = self.client.get(
            reverse("songs-all", kwargs={"version": "v1"})
        )
        # fetch the data from db
        expected = Songs.objects.all()
        serialized = SongSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
