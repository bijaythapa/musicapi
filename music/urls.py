from django.urls import path, re_path
from .views import ListLongView, LoginView

urlpatterns = [
    path('songs/', ListLongView.as_view(), name='songs-all'),
    path('auth/login/', LoginView.as_view(), name="auth-login"),
]
